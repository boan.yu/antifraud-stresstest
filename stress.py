from gevent import monkey
monkey.patch_all()

import gevent
import os
import random
import requests
import time
import sys
import signal
import json

from shopee_deploy.utils import init_logger, get_domain_suffix
log = init_logger()

successes = 0
errors = 0
exceptions = 0

stop = False


def stop_system(signum, frame):
	global stop
	stop = True


signal.signal(signal.SIGINT, stop_system)


def worker(cid, worker_id, userid):
	# print(str(cid) + "---" + str(worker_id) + "---" + str(userid))
	# example_domain = 'shopee.%s' % get_domain_suffix(cid)  # This will generate shopee.sg if cid is sg, and shopee.co.id if cid is id. This is not used because in this example we are targetting shopee.io.
	# log.info('worker_started|worker_id=%s,userid=%s', worker_id, userid)
	global successes
	global errors
	global exceptions
	err_rate = 0.1
	s = requests.Session() # Use requests.Session whenever possible. This will use keepalive connections which is a lot faster and does not deplete client port.
	url = 'http://fraudcheck-api.test.shopee.io/checkorders'
	body = '{"userid": %d,' \
		   '"country": "XX",' \
		   '"behavior_type": 0,' \
		   '"orders": [' \
		   '	{' \
		   '		"order": {' \
		   '			"shopid": 123456782' \
		   '				},' \
		   '		"items": [' \
		   '			{' \
		   '				"itemid": 2421279125,' \
		   '				"modelid": 6583302391,' \
		   '				"amount": 1,' \
		   '				"order_price": 900000,' \
		   '				"currency": "SGD",' \
		   '				"offerid": 0' \
		   '					}' \
		   '				],' \
		   '		"carrier_id": [' \
		   '				79900,' \
		   '				70022,' \
		   '				70020,' \
		   '				70021,' \
		   '				70015,' \
		   '				70016],' \
		   '		"tax_address": ""' \
		   '	}' \
		   '		],' \
		   '"is_credit_card": false,' \
		   '"total_price": 1060000000,' \
		   '"deviceid": "QzFCNTJCNzg1MzhBNERGMjg0OEUxRkFDNEM3MEQ5MjA=",' \
		   '"device_fingerprint": "%s",' \
		   '"tongdun_blackbox": "xxxxxx",' \
		   '"user_agent_type": 1,' \
		   '"app_version_name": "24424",' \
		   '"rn_bundle_name": "1568283947",' \
		   '"voucher_code": "gfgfhfhgf",' \
		   '"promotionid": 123,' \
		   '"vouchers": [	' \
		   '				{	' \
		   '					"promotionid": 123,	' \
		   '					"shopid" : 123,	' \
		   '					"voucher_code" : "",	' \
		   '					"reward_type" : 0	' \
		   '				}	' \
		   '			]' \
		   '}' % userid
	headers = {'Content-Type':'application/json'}
	# print(body)
	# r = requests.post(url, data=body, headers=headers)
	# log.exception(r.text)
	
	# print(r.text)
	while not stop:
		r = requests.post(url,data=body,headers=headers)
		log.exception(r.text)
		# r = random.random()
		# if r > err_rate:
		# 	url = 'https://sstest.shopee.io/apis/target/v1/basic/success?userid=%s' % userid
		# else:
		# 	url = 'https://sstest.shopee.io/apis/target/v1/basic/error?userid=%s' % userid
		# try:
		# 	resp = s.get(url)
		# 	if resp.status_code == 200:
		# 		successes += 1
		# 	else:
		# 		errors += 1
		# except:
		# 	log.exception('Exception encountered while making request')
		# 	exceptions += 1



def logger_worker():
	global successes
	global errors
	global exceptions
	while not stop:
		log.info('successes=%s,errors=%s,exceptions=%s' % (successes, errors, exceptions))
		time.sleep(1)


def main():
	cid = sys.argv[1]  # You can use this if you need it
	concurrency = int(sys.argv[2])
	# cid = 'sg'
	# concurrency=3


	# If you are testing on local, you can use something like `INDEX=5 python stress.py sg 3` to simulate a container of index 5.
	# container_id = int(os.environ.get('INDEX', 0))
	container_id = 5
	start = container_id * concurrency
	stop = start + concurrency
	from data.userids import userids
	# The line below will create a new small slice, and release the original large slice. This will eventually use less memory so you can load more things after this line. However, if original large slice + new small slice cannot fit in memory, you will get an OOM.
	userids = userids[start:stop]

	threads = []
	for i in range(concurrency):
		threads.append(gevent.spawn(worker, cid, i, userids[i]))
	threads.append(gevent.spawn(logger_worker))
	gevent.joinall(threads)


if __name__=='__main__':
	main()
